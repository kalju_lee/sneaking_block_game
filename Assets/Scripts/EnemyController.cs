﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public GameObject player;
    public float rotationSpeed = 1;
    public bool willScan;
    public float scanPeriod = 3f;
    public GameController gameController;

    RaycastHit hitInfo;
    Vector3 dirVector;
    public Vector3 lastKnownPosition;
    GunScript gun;
    public GameObject muzzle;
    EnemyMovement movement;
    EnemyHealth health;
    Animator anim;

    const int ENGAGE = 0;
    const int HUNT = 1;
    const int SCAN = 2;
    const int PATROL = 3;
    const int NOTICE = 4;
    const int WAIT = 5;

    public int[] enemyStates = {ENGAGE, HUNT, SCAN, PATROL, NOTICE};
    public int currentEnemyState;


    public bool InConeOfVision(int angle) {
        return (Vector3.Angle(transform.forward, dirVector) < angle);
    }

    public bool PossibleLOS() {
        return (Physics.Linecast(transform.position, player.transform.position, out hitInfo) 
            && hitInfo.collider.gameObject.tag == "Player");
    }

    void TurnTowardsTarget() {
        Quaternion originalRotation = transform.rotation;
        // Quaternion towardsPlayer = Quaternion.LookRotation(dirVector);
        Quaternion towardsPlayer = gun.barrelDirection(player.transform.position);
        transform.rotation = Quaternion.Lerp(originalRotation, towardsPlayer, Time.deltaTime * rotationSpeed);
    }

    public void ShotFired(Transform location) {
        if ((Vector3.Distance(location.position, transform.position) > 1f) 
            && (Vector3.Distance(transform.position, player.transform.position) < 10)) {
            currentEnemyState = HUNT;
            lastKnownPosition = location.position;
        }


    }

    void Awake() {
    }

    // Use this for initialization
    void Start() {
        gun = muzzle.GetComponent<GunScript>();
        movement = GetComponent<EnemyMovement>();
        health = GetComponent<EnemyHealth>();
        anim = GetComponent<Animator>();
        currentEnemyState = PATROL;
        willScan = false;
    }
	
    // Update is called once per frame
    void Update() {

        if (player.GetComponent<PlayerHealth>().isDead) {
            currentEnemyState = PATROL;
            gun.fireCommand = false;
            return;
        }
        //      Debug.Log("state: " + currentEnemyState);
        //      Debug.Log("plos: " + PossibleLOS());


        if (health.isDead) {
            gun.fireCommand = false;
            return;
        }

        dirVector = player.transform.position - transform.position;
        int lastEnemyState = currentEnemyState;

        if (PossibleLOS()) {
            if (InConeOfVision(90)) {
                TurnTowardsTarget();
                lastKnownPosition = player.transform.position;
            }
            if (InConeOfVision(10)) {
                currentEnemyState = ENGAGE;
                willScan = true;
                lastKnownPosition = player.transform.position;
                gun.fireCommand = true;
            } else {
                currentEnemyState = HUNT;
                gun.fireCommand = false;
                if (Vector3.Distance(transform.position, lastKnownPosition) <= 0.6)
                    currentEnemyState = SCAN;
            }

        } else {

            gun.fireCommand = false;

            if (currentEnemyState == ENGAGE) {
                currentEnemyState = HUNT;
            } else if (currentEnemyState == HUNT && Vector3.Distance(transform.position, lastKnownPosition) <= 0.6) {
                //      Debug.Log("code reached scan state change");
                currentEnemyState = SCAN;       
            }
        }
        //   if (lastEnemyState != currentEnemyState)
        //    Debug.Log("state: " + currentEnemyState);
    }
}
