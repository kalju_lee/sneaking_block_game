﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int bodyHitDamage;
    public int armorHitDamage;

    public bool isDead;
    ParticleSystem pop;
    // public bool isSinking;

    public void bodyHit() {
        TakeDamage(bodyHitDamage);
    }

    public void armorHit() {
        TakeDamage(armorHitDamage);
    }

    public void TakeDamage(int amount) {
        if (isDead)
            return;
        currentHealth -= amount;
        Debug.Log(amount + " Damage taken\n" + currentHealth + " health left");
        if (currentHealth <= 0)
            Death();
    }

    public void Death() {
        isDead = true;
        pop.Play();
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        foreach (MeshRenderer rend in gameObject.GetComponentsInChildren<MeshRenderer>())
            rend.enabled = false;
        //gameObject.GetComponent<EnemyMovement>().enabled = false;

    }

    void Awake() {
        currentHealth = startingHealth;
        pop = GetComponentInChildren<ParticleSystem>();
        Debug.Log(pop);
        isDead = false;
    }

    // Use this for initialization
    void Start() {
	
    }
	
    // Update is called once per frame
    void Update() {
        if (isDead && pop.isStopped)
            Destroy(gameObject);
    }
}
