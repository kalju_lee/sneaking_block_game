﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(NavMeshAgent))]
public class EnemyMovement : MonoBehaviour {
    public Transform[] targets;
    public int waitTime = 0;
    //  public float rotationSpeed;
    bool isWaiting;
    public float stopWaitingTime;
    float phaseTime = 1;
    public float period = 0.5f;
    public float angle = 90;
    float stopScanningTime;
    Vector3 destination;
    NavMeshAgent agent;
    EnemyController controller;
    Quaternion originalRotation;
    float movementTimeOut;
    Vector3 lastEnemyPosition = new Vector3(0, 0, 0);


    Transform currentTarget;
    int targetIndex;

    const int ENGAGE = 0;
    const int HUNT = 1;
    const int SCAN = 2;
    const int PATROL = 3;
    const int NOTICE = 4;
    const int WAIT = 5;
    
    public int[] enemyStates = {ENGAGE, HUNT, SCAN, PATROL, NOTICE, WAIT};
        
    void Start() {
        // Cache agent component and destination
        agent = GetComponent<NavMeshAgent>();
        controller = GetComponent<EnemyController>();
        destination = agent.destination;
        targetIndex = 0;
        currentTarget = targets [0];
        isWaiting = false;
        stopWaitingTime = 0;
    }

    bool isInArray(Transform[] array, Transform thing) {
        for (int i = 0; i < array.Length; i++)
            if (thing == array [i])
                return true;
        return false;

    }
        
    void OnTriggerEnter(Collider other) {
        if (controller.currentEnemyState == PATROL) {
            // if (!isInArray(targets, other.transform))
            //   Debug.Log("target index: " + targetIndex);
            //  if (other.tag != "Waypoint")
            if (!isInArray(targets, other.transform))
                return;
            targetIndex++;
            if (targetIndex == targets.Length)
                targetIndex = 0;
            currentTarget = targets [targetIndex];
            isWaiting = true;
            stopWaitingTime = Time.time + waitTime;

        }
    }

    void ScanRotation() {
        phaseTime = phaseTime + Time.deltaTime;
        float phase = Mathf.Sin(phaseTime / period);
        transform.localRotation = Quaternion.Euler(new Vector3(0, (phase * angle) + 0.01f, 0)) * originalRotation;
    }

    void checkForHang() {
        Debug.Log(movementTimeOut);
        //   Debug.Log(transform.position);
        if (Vector3.Distance(lastEnemyPosition, transform.position) > 0.3) {
            lastEnemyPosition = transform.position;
            movementTimeOut = Time.time + 4f;
        } else {
            if (movementTimeOut < Time.time)
                controller.currentEnemyState = PATROL;
        }
    }


    void Update() {
        // Update destination if the target moves one unit


        switch (controller.currentEnemyState) {

            case PATROL: 
                if (isWaiting == true && stopWaitingTime < Time.time)
                    isWaiting = false;
                if (isWaiting == false)
                if (Vector3.Distance(destination, currentTarget.position) > 0) {
                    destination = currentTarget.position;
                    agent.destination = destination;
                }
                break;
            case ENGAGE: 
                agent.destination = transform.position;
                break;

            case HUNT:
               // destination = controller.lastKnownPosition;
                if (Vector3.Distance(destination, controller.lastKnownPosition) > 0) {
                    destination = controller.lastKnownPosition;
                    agent.destination = destination;
                } 
              //  transform.Rotate(new Vector3(0, 0, 90) * Time.deltaTime * rotationSpeed);
                break;

            case SCAN:
            //    Debug.Log("sst when entering scan: " + stopScanningTime);

                if (controller.willScan) {
                    originalRotation = transform.rotation;
                    stopScanningTime = controller.scanPeriod + Time.time;
                    //   Debug.Log("sst after successful willscan check: " + stopScanningTime);
                    controller.willScan = false;
                }
                if (stopScanningTime > Time.time) {
                    if (controller.PossibleLOS() && controller.InConeOfVision(75))
                        controller.currentEnemyState = ENGAGE;
                    else
                        ScanRotation();
                } else
                    controller.currentEnemyState = PATROL;            
                break;
            default:
                Debug.Log("bad enemy state case");
                break;
        }

        checkForHang();
    }
}
