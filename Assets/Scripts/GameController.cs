﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject player;
    public GameObject exit;
    public GameObject[] enemyList;
    public Text endText;
    public int nextLevelIndex;
    List<EnemyController> enemyControllers = new List<EnemyController>();
    List<EnemyHealth> enemyHealths = new List<EnemyHealth>();

    PlayerHealth playerHealth;
    bool gameOverMan = false;


    // Use this for initialization
    void Start() {
        playerHealth = player.GetComponent<PlayerHealth>();
        endText.enabled = false;
        foreach (GameObject thing in enemyList) {
            enemyControllers.Add(thing.GetComponent<EnemyController>());
            enemyHealths.Add(thing.GetComponent<EnemyHealth>());
        }
        if (enemyControllers.Count == 0)
            Debug.Log("enemyController list empty");
    }
	
    // Update is called once per frame
    void Update() {
	
        if (playerHealth.isDead)
            GameOver(false);

        if (playerHealth.gameWon)
            GameOver(true);

        if (gameOverMan && Input.GetKey(KeyCode.R))
        if (playerHealth.isDead)
            Application.LoadLevel(Application.loadedLevel);
        else
            Application.LoadLevel(nextLevelIndex);

    }

    public void ShotFiredSwitchBoard(Transform location) {
        foreach (EnemyController thing in enemyControllers)
            thing.ShotFired(location);
    }


    void GameOver(bool winCondition) {
        gameOverMan = true;
        if (winCondition) {
            endText.text = "YOU WIN\nPRESS 'R' TO PROCEED"; 
        } else {
            endText.text = "YOU LOSE\nPRESS 'R' TO RESTART";
        }
        endText.enabled = true;

        foreach (EnemyHealth enemy in enemyHealths)
            enemy.isDead = true;

    }
}
