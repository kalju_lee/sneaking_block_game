﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunScript : MonoBehaviour {

    public float timeBetweenBullets = 0.25f;
    public float spread = 0.1f;
    public bool fireCommand;
    public GameObject projectile;
    public GameController gameController;
    public int ammo = 30;
    public int ammoPerShot = 1;
    public Text guiAmmoCount;
    
    float timer;
    ParticleSystem gunParticles;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    
    void Awake() {
        //   gunParticles = GetComponent<ParticleSystem>();
        gunAudio = GetComponent<AudioSource>();
        gunLight = GetComponent<Light>();
        fireCommand = false;
    }
    
    // Use this for initialization
    void Start() {
        
    }
    
    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime;
        if (fireCommand && timer >= timeBetweenBullets && Time.timeScale != 0 && ammo > 0) {
            Shoot();
        } 
        if (timer >= timeBetweenBullets * effectsDisplayTime)
            DisableEffects();
    }
    
    public void DisableEffects() {
        gunLight.enabled = false;
    }
    
    void Shoot() {
        timer = 0f;
        gunAudio.Play();
        gunLight.enabled = true;
        //   gunParticles.Stop();
        //   gunParticles.Play();

        Quaternion originalRotation = transform.rotation;
        transform.rotation = Quaternion.Euler(SprayGenerator(spread)) * originalRotation;
        Instantiate(projectile, transform.position, transform.rotation);
        transform.rotation = originalRotation;
        gameController.ShotFiredSwitchBoard(transform);
        ammo -= ammoPerShot;
        if (guiAmmoCount != null)
            guiAmmoCount.text = ammo.ToString();
        
    }

    Vector3 SprayGenerator(float spread) {
        return new Vector3(Random.value * spread, Random.value * spread, Random.value * spread);
    }

    public Quaternion barrelDirection(Vector3 target) {
        Vector3 dirVector = target - transform.position;
        return Quaternion.LookRotation(dirVector);
    }
}
