﻿using UnityEngine;
using System.Collections;

public class HitToBody : MonoBehaviour {

    EnemyHealth enemy;
    
    void OnTriggerEnter(Collider other) {
        if (other.tag == "Projectile") {
            if (Vector3.Angle(transform.forward, other.transform.forward) > 90)
                enemy.armorHit();
            else
                enemy.bodyHit();
        }
    }
    
    void Awake() {
        enemy = GetComponent<EnemyHealth>();
    }
    // Use this for initialization
    void Start() {
        
    }
    
    // Update is called once per frame
    void Update() {
        
    }
}
