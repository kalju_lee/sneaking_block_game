﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public int startingHealth = 100;
    public int currentHealth;
    public Text healthText;
    PlayerMovement playerMovement;
    public bool isDead;
    public int damageTakenPerShot = 25;
    bool damaged;
    public bool gameWon = false;
    ParticleSystem pop;


    void Awake() {
        playerMovement = GetComponent<PlayerMovement>();
        currentHealth = startingHealth;
        pop = GetComponentInChildren<ParticleSystem>();
        isDead = false;
    }
    // Use this for initialization
    void Start() {
	
    }
	
    // Update is called once per frame
    void Update() {
	
        if (isDead)
            return;
        healthText.text = currentHealth.ToString();

        if (currentHealth <= 0) {
            isDead = true;
            Dead();
        }

    }


    void OnTriggerEnter(Collider other) {
        if (other.tag == "Projectile") {
            currentHealth -= damageTakenPerShot;
        }

        if (other.tag == "Exit")
            gameWon = true;
    }

    void Dead() {
        playerMovement.gun.fireCommand = false;
        playerMovement.enabled = false;
        pop.Play();
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        foreach (MeshRenderer rend in gameObject.GetComponentsInChildren<MeshRenderer>())
            rend.enabled = false;
    }
}
