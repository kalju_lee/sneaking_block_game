﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    Rigidbody body;
    public float speed;
    public float rotationSpeed = 1;
    public GameObject muzzle;
    public GunScript gun;
    Vector3 movement;

    // Use this for initialization
    void Start() {
        body = GetComponent<Rigidbody>();
        if (body == null)
            Debug.Log("body = null");
        gun = muzzle.GetComponent<GunScript>();
    }
	
    // Update is called once per frame
    void Update() { 
	
        gun.fireCommand = Input.GetButton("Fire1");

    }

    void FixedUpdate() {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        float rotation = Input.GetAxisRaw("Mouse X");
        Move(h, v);

        //   if (rotation != 0) {
        transform.Rotate(0, rotation * rotationSpeed * Time.deltaTime, 0);
        // }
    } 

    void Move(float h, float v) {
        movement.Set(h, 0.0f, v);
        movement = transform.TransformDirection(movement);
        movement = movement.normalized * speed * Time.deltaTime;
        body.MovePosition(transform.position + movement);
    }

}
