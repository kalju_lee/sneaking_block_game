﻿using UnityEngine;
using System.Collections;

public class RoundMover : MonoBehaviour {

    public float speed;
    public string[] targetTags;
    public int damage;


    void OnTriggerEnter(Collider other) {
        Destroy(gameObject);
    }

    void OnTriggerExit(Collider other) {
        Destroy(gameObject);
    }

    // Use this for initialization
    void Start() {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    // Update is called once per frame
    void Update() {
	
        if (Mathf.Abs(transform.position.y) > 2)
            Destroy(gameObject);

    }
}
