# Sneaking Block #

I made this in 2015 as a way to familiarize myself with Unity and C#.  After a couple of tutorials, I dove right into making a small game from scratch.  I had just finished playing Hotline Miami and wanted to make something with a similar feel.  
The AI is in the form a decision tree based on line-of-sight and other agent behaviors, and results in a sort of chicken-like behavior that I like, despite not having been the intention.
I basically started writing without any clear plan, and it shows in the code: it's a rat's nest of data-shuffling.  Biggest lesson learned: planning is important.

If the files are downloaded and imported into Unity as a project, it can be built from there.  If creating a stand-alone build (not just running in Unity), there are three scenes to include- 1, 2, and 'game over'.